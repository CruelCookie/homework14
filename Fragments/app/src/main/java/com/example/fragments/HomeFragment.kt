package com.example.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.fragments.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    private val adapter = TextRecViewAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater)
        binding.bottomNavig.selectedItemId = R.id.home
        binding.bottomNavig.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.profile -> {
                    Navigation.findNavController(binding.root).navigate(R.id.nav_homeFrag_to_profFrag)
                }
                R.id.home -> { }
                R.id.web -> {
                    Navigation.findNavController(binding.root).navigate(R.id.nav_homeFrag_to_webFrag)
                }
            }
            true
        }
        init()
        return binding.root
    }

    private fun init(){
        binding.apply{
            rcTextView.layoutManager = LinearLayoutManager(this.root.context)
            rcTextView.adapter = adapter
            btAddView.setOnClickListener{
                adapter.addTextRec(TextRec((etView.text.toString()), R.style.MyTextView1))
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }
}