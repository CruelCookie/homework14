package com.example.fragments

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class DataModel: ViewModel() {
    val data: MutableLiveData<Profile> by lazy{
        MutableLiveData<Profile>()
    }
    val current: MutableLiveData<Profile> by lazy{
        MutableLiveData<Profile>()
    }
}