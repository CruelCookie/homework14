package com.example.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.Navigation
import com.example.fragments.databinding.FragmentProfileBinding

class ProfileFragment : Fragment() {
    private val dataModel: DataModel by activityViewModels()
    private lateinit var binding: FragmentProfileBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(inflater)
        binding.bottomNavig.selectedItemId = R.id.web
        binding.bottomNavig.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.web -> {
                    Navigation.findNavController(binding.root).navigate(R.id.nav_profFrag_to_webFrag)
                }
                R.id.profile -> { }
                R.id.home -> {
                    Navigation.findNavController(binding.root).navigate(R.id.nav_profFrag_to_homeFrag)
                }
            }
            true
        }
        binding.btExit.setOnClickListener{
            Navigation.findNavController(binding.root).navigate(R.id.nav_profFrag_to_logFrag)
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        dataModel.current.observe(activity as LifecycleOwner) {
            binding.tvUName.text = it.name
            binding.tvUBirth.text = it.birthday
        }
    }
    companion object {

        @JvmStatic
        fun newInstance() = ProfileFragment()
    }
}