package com.example.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.Navigation
import com.example.fragments.databinding.FragmentLoginBinding
import java.io.IOException
import java.nio.charset.Charset

var users = mutableListOf<Profile>()
var logins = ArrayList<String>()
var passwords = ArrayList<String>()

class LoginFragment : Fragment() {
    private val dataModel: DataModel by activityViewModels()
    private lateinit var binding: FragmentLoginBinding
/*
    private val users: ArrayList<Profile> = ArrayList()
    private val logins: ArrayList<String> = ArrayList()
    private val passwords: ArrayList<String> = ArrayList()
*/
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentLoginBinding.inflate(inflater)
        binding.bottomNavig.isVisible = false;
        users.add(Profile("admin", "admin", "John", "15/09/2002"))
        logins.add("admin")
        passwords.add("admin")
/*
        try{
            val obj = JSONObject(getJSONFromAssets()!!)
            val userArray = obj.getJSONArray("users")
            for(i in 0 until userArray.length()){
                val user = userArray.getJSONObject(i)
                val name = user.getString("name")
                val login = user.getString("login")
                val password = user.getString("password")
                val birthday = user.getString("birthday")

                val userDetails = Profile(login, password, name, birthday)
                users.add(userDetails)
                logins.add(login)
                passwords.add(password)
            }
        }
        catch (e: JSONException){
            e.printStackTrace()
        }
*/
        dataModel.data.observe(activity as LifecycleOwner) {
            users.add(it)
            passwords.add(it.password)
            logins.add(it.login)
        }
        binding.tvLogin.setOnClickListener{
            Navigation.findNavController(binding.root).navigate(R.id.nav_logFrag_to_regFrag)
        }
        binding.btLogin.setOnClickListener{
            if(binding.etLLogin.text.isNotEmpty() and binding.etLPassword.text.isNotEmpty()) {
                if ((binding.etLLogin.text.toString() in logins) and (binding.etLPassword.text.toString() in passwords)) {
                    for (i in users) {
                        if(i.login == binding.etLLogin.text.toString()){
                            dataModel.current.value = i
                            Navigation.findNavController(binding.root).navigate(R.id.nav_logFrag_to_homeFrag)
                        }
                    }
                }else{
                    Toast.makeText(binding.root.context,"Incorrect login or password",Toast.LENGTH_SHORT).show()
                }
            } else{
                Toast.makeText(binding.root.context,"Empty login or password", Toast.LENGTH_SHORT).show()
            }
        }
        return binding.root
    }
/*
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        dataModel.data.observe(activity as LifecycleOwner) {
            users.add(it)
            passwords.add(it.password)
            logins.add(it.login)
            //dataModel.users.value = users
            Log.i("Login", "${it.login}")
            /*
            File("users.txt").printWriter().use { out ->
                out.println("{\n\"users\":[")
                users.forEach {
                    out.print("{\"login\":${it.login},\"name\"${it.name},\"password\"${it.password},\"birthday\"${it.birthday}}")
                    if(it != users.last()){
                        out.println(",")
                    }
                }
                out.println("]\n}")
            }*/
        }
    }*/
    private fun getJSONFromAssets(): String? {

        var json: String? = null
        val charset: Charset = Charsets.UTF_8
        try {
            val myUsersJSONFile = requireActivity().assets.open("users.json")
            val size = myUsersJSONFile.available()
            val buffer = ByteArray(size)
            myUsersJSONFile.read(buffer)
            myUsersJSONFile.close()
            json = String(buffer, charset)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return json
    }
    companion object {
        @JvmStatic
        fun newInstance() = LoginFragment()
    }
}