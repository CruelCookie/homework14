package com.example.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import com.example.fragments.databinding.FragmentRegistrationBinding

class RegistrationFragment : Fragment() {
    private val dataModel: DataModel by activityViewModels()
    private lateinit var binding: FragmentRegistrationBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegistrationBinding.inflate(inflater)

        binding.tvRegistration.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.nav_regFrag_to_logFrag)
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.btRegistration.setOnClickListener{
            if(binding.etLogin.text.isNotEmpty() and binding.etPassword.text.isNotEmpty()
                and binding.etName.text.isNotEmpty() and binding.etBirth.text.isNotEmpty()) {
                dataModel.data.value = Profile(
                    binding.etLogin.text.toString(), binding.etPassword.text.toString(),
                    binding.etName.text.toString(), binding.etBirth.text.toString()
                )
                Navigation.findNavController(binding.root).navigate(R.id.nav_regFrag_to_logFrag)
            }
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() = RegistrationFragment()
    }
}