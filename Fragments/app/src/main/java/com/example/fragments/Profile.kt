package com.example.fragments

data class Profile(val login: String, val password: String, val name: String, val birthday: String)
